# Immigrada

## Description

Small script for converting entries from [Blogger.com](https://draft.blogger.com/) blogging service to markdown files for [Middleman](http://middlemanapp.com/) static site generator. It should be also possible to use the result with other site generators.

Script parses Blogger Atom XML file for blog entries and saves each of them to separate file in markdown format. It also downloads images from posts and replaces links to them with local paths.

## Installation

### From RubyGems

    gem install immigrada

### Manual gem building and installation

1. Clone this project and navigate to it;
1. Build gem with `rake package` command;
1. Install it with `gem install pkg/immigrada-0.2.gem`;

## Usage

Use the following command to convert your blog:

    immigrada run -i blog.xml -o output
    
Where:

- "blog.xml" is Blogger XML backup, if you don't know where to get it, read next section;
- "output" - output directory (optional parameter, will be `out/` if you omit it).

### Exporting your blog

1. Sign in to your Blogger account;
1. Go to Settings - Other;
1. Choose "Export blog" in "Blog tools" section;
1. Download XML file with exported entries;

You can find more instructions [here](https://support.google.com/blogger/answer/97416?hl=en).

### Manual script execution (without installation)

1. Install Ruby if needed;
1. Clone this project and navigate to it;
1. Run `bundle install` to download dependiencies;
1. Run `ruby bin/immigrada run -i blog.xml` where "blog.xml" is Blogger XML backup, downloaded in previous section.

## To Do

There are still a lot of things to do, feel free to contribute:

 - Resolve problems with line breaks when parsing 'pre' elements;
 - Improve status messages while processing;
 - Add proper error handling;
 - Add input file checks for correctness;
 - Add support for Tumblr;
 - Improve test coverage;
 
## License

Project is distributed under [The MIT License](http://opensource.org/licenses/MIT).

