Gem::Specification.new do |s|
  s.name = 'immigrada'
  s.version = '0.2'
  s.platform = Gem::Platform::RUBY
  s.authors = ['Michail Ovchinnikov']
  s.email = 'michail@ovchinnikov.cc'
  s.homepage = 'https://github.com/movch/immigrada'
  s.summary = 'Utility for migration from Blogger.com to static blogs generator.'
  s.description = %q{Script parses Blogger Atom XML file for blog entries and 
                     saves each of them to separate file in markdown format. 
                     It also downloads images from posts and replaces links to 
                     them with local paths.}
  
  s.rubyforge_project = 'immigrada'

  s.files = Dir.glob('{bin,lib}/**/*') + %w{README.md}
  s.executables = ['immigrada']
 
  s.add_dependency 'nokogiri'
  s.add_dependency 'reverse_markdown'
  s.add_dependency 'gli'
end
