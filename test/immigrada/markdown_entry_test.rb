require_relative '../test_helper'

class TestMarkdownEntry < Minitest::Test
  def setup
    fixture_xml = File.expand_path('../fixtures/entry.xml', File.dirname(__FILE__))
    fixture = File.read(fixture_xml)
    test_entry = Nokogiri::XML
                 .parse(fixture)
                 .remove_namespaces!
                 .xpath('entry')
                 .first
    b_entry = Immigrada::BloggerEntry.new(test_entry)
    @entry = Immigrada::MarkdownEntry.new(b_entry, './out')
  end

  def test_post_processing
    test_post = %(---\ntitle: Test title\ndate: 2014-08-12\ntags: tag1,tag2\n---\nContent body\n)
    assert_equal test_post, @entry.combine_post
  end

  def test_filename_generation
    assert_equal '2014-08-12-blog-post.markdown', @entry.combine_filename
  end
end
