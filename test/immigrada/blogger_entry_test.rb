require_relative '../test_helper'

class TestEntry < Minitest::Test
  def setup
    fixture_xml = File.expand_path('../fixtures/entry.xml', File.dirname(__FILE__))
    fixture = File.read(fixture_xml)
    test_entry = Nokogiri::XML
                 .parse(fixture)
                 .remove_namespaces!
                 .xpath('entry')
                 .first
    @entry = Immigrada::BloggerEntry.new(test_entry)
  end

  def test_check_if_entry_is_post
    assert_equal true, @entry.post?
  end

  def test_title
    assert_equal 'Test title', @entry.title
  end

  def test_date
    assert_equal '2014-08-12', @entry.published
  end

  def test_tags
    assert_equal %w(tag1 tag2), @entry.tags
  end

  def test_slug
    assert_equal 'blog-post', @entry.slug
  end

  def test_content
    assert_equal 'Content body', @entry.content
  end
end
