module Immigrada
  ##
  # Class for parsing post data from entry XML object.
  class BloggerEntry
    def initialize(entry_xml)
      @entry_xml = entry_xml
    end

    # Returns string with entry title.
    def title
      @entry_xml
        .search('title')
        .text
    end

    # Returns string with published date.
    def published
      raw_date = @entry_xml
                 .search('published')
                 .text
      Date
        .parse(raw_date)
        .strftime('%Y-%m-%d')
        .to_s
    end

    # Returns array of entry tags.
    def tags
      @entry_xml
        .search('category[@scheme=\'http://www.blogger.com/atom/ns#\']')
        .map { |c| c['term'] }
    end

    # Returns string with entry slug.
    def slug
      link = @entry_xml
             .search("link[@rel='alternate']")
             .first['href']
      File.basename(link, File.extname(link))
    end

    # Returns string with entry HTML content.
    def content
      @entry_xml
        .search("content[@type='html']")
        .text
    end

    # Check if entry is post.
    def post?
      @entry_xml
        .search('id')
        .text
        .include?('post')
    end
  end
end
