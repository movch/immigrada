module Immigrada
  ##
  # Class for generation markdown file with post from entry object.
  class MarkdownEntry
    def initialize(b_entry, out_dir)
      @b_entry = b_entry
      @out_dir = out_dir
      @content = @b_entry.content
    end

    # Saves post contents to file.
    def save
      filename = combine_filename
      path = File.join(@out_dir, filename)
      File.open(path, 'w') { |f| f.write(combine_post) }
      puts "Post '#{@b_entry.title}' saved to: #{filename}"  
    end

    # Returns string with post: frontmatter and content.
    def combine_post(params={})
      <<-EOF.gsub(/^\s+/, '')
        ---
        title: #{@b_entry.title}
        date: #{@b_entry.published}
        tags: #{@b_entry.tags.join(',')}
        ---
        #{ReverseMarkdown.convert(@content)}
      EOF
    end

    # Returns string with filename combined from date and slug.
    def combine_filename
      "#{@b_entry.published}-#{@b_entry.slug}.markdown"
    end
  end
end

