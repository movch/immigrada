module Immigrada
  ##
  # Decorator module to replace remote images in post
  # with their downloaded copies. Extends MarkdownEntry class.
  module LocalImagesDecorator
    def combine_post
      post_dir = "#{@b_entry.published}-#{@b_entry.slug}"
      @content = insert_local_images(@content, post_dir)
      super
    end

    private
    
    # Finds images, downloads them, and replaces links to them
    # with local copies.
    def insert_local_images(entry_html, post_filename)
      html_doc = Nokogiri::HTML.parse(entry_html)
      html_doc.xpath('//a[@href]/img[@src]').each do |img|
        img_url = img.parent['href']
        post_img_dir = "#{@out_dir}/#{post_filename}"
        FileUtils.mkdir(post_img_dir) unless File.directory?(post_img_dir)
        img_filename = File.basename(img_url)
        img_local_file = "#{post_img_dir}/#{img_filename}"
        download(img_url, img_local_file)
        img.parent.replace("<img src='/#{post_filename}/#{img_filename}'/>")
        img.remove
      end
      html_doc
    end

    # Helper method for downloading files.
    def download(url, filename)
      File.open(filename, 'wb') do |saved_file|
        open(url, 'rb') do |read_file|
          saved_file.write(read_file.read)
        end
      end
    end
  end
end

