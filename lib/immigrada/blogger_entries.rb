module Immigrada
  ##
  # Class for loading and processing blog entries from Blogger backup file.
  class BloggerEntries
    # Processes each entry object and saves it to file in markdown format.
    def process(input_file, out_dir)
      entries = load(input_file)
      entries.each do |e|
        next unless e.post?
        m_entry = MarkdownEntry.new(e, out_dir)
        m_entry.extend(LocalImagesDecorator)
        FileUtils.mkdir_p(out_dir) unless File.directory?(out_dir)
        m_entry.save
      end
    end

    private

    # Reads Blogger backup file and parses it for entries. Returns array
    # of entry objects.
    def load(file)
      Nokogiri::XML
        .parse(File.read(file))
        .remove_namespaces!
        .xpath('/feed/entry')
        .map { |e| BloggerEntry.new(e) }
    end
  end
end
